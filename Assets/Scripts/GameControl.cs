﻿using UnityEngine;
using UnityEngine.UI;

namespace game
{
    public struct PosBoll
    {
        public int x;
        public int y;
    }

    public class GameControl : MonoBehaviour
    {
        [SerializeField] [Range(6, 10)] private int _sizeBord = 6;
        [SerializeField] [Range(4, 7)] private int _countBolls = 4;
        [SerializeField] [Range(1, 10)] private float _speed = 2f;

        [SerializeField] private Boll[] _bolls;
        [SerializeField] private Transform _rootTransform;
        [SerializeField] private AddPointTextView _addPointTextView;
        [SerializeField] private Text _textPoint;
        private int points = 0;

        private Boll _selectBoll;
        private PosBoll _selectPos;
        private PosBoll _posFlipBoll1;
        private PosBoll _posFlipBoll2;

        private bool _flagAnimation;

        private Boll[,] _bord;

        void Start()
        {
            _bord = new Boll[_sizeBord, _sizeBord];
            for (int i = 0; i < _sizeBord; i++)
                for (int j = 0; j < _sizeBord; j++)
                    AddNewBoll(i, j, Vector2.zero, "");

        }
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && _flagAnimation == false)
                SelectBoll();

            _flagAnimation = false;
            Animation();

            if (!_flagAnimation)
            {
                int point = TestLine();
                if (point > 0)
                {
                    points += point;
                    _textPoint.text = "points: " + points;
                }
            }


            if (!_flagAnimation)
            {
                if (_posFlipBoll1.x != -1)
                {
                    FlipBoll(_posFlipBoll1, _posFlipBoll2);
                    _posFlipBoll1.x = -1;
                }
                else
                    DownBoll();
            }
            if (!_flagAnimation)
            {
                if (TestEndGame())
                {
                    points = 0;
                    _addPointTextView.SetText("END GAME", 5, true);
                    int countLoop = _sizeBord * _sizeBord / 2;
                    while (countLoop > 0)
                    {
                        PosBoll pos1;
                        PosBoll pos2;
                        pos1.x = Random.Range(0, _sizeBord);
                        pos1.y = Random.Range(0, _sizeBord);
                        pos2.x = Random.Range(0, _sizeBord);
                        pos2.y = Random.Range(0, _sizeBord);
                        FlipBoll(pos1, pos2);
                        countLoop--;
                    }
                }
            }
        }

        private void DownBoll()
        {
            bool flagDrop = false;
            for (int i = 0; i < _sizeBord; i++)
                for (int j = 0; j < _sizeBord; j++)
                {
                    if (_bord[i, j] == null)
                        continue;

                    if (_bord[i, j].del && _bord[i, j].transform.localScale.x == 0)
                    {
                        GameObject.Destroy(_bord[i, j].gameObject);
                        _bord[i, j] = null;
                        flagDrop = true;
                    }
                }
            while (flagDrop)
            {
                _flagAnimation = true;
                flagDrop = false;
                for (int i = 0; i < _sizeBord; i++)
                    for (int j = 0; j < _sizeBord - 1; j++)
                    {
                        if (_bord[i, j] == null && _bord[i, j + 1] != null)
                        {
                            _bord[i, j] = _bord[i, j + 1];
                            _bord[i, j].pos.x = i;
                            _bord[i, j].pos.y = j;
                            _bord[i, j + 1] = null;
                            _bord[i, j].offset += new Vector3(0, 1, 0);
                            flagDrop = true;
                        }
                    }
                for (int i = 0; i < _sizeBord; i++)
                {
                    if (_bord[i, _sizeBord - 1] == null)
                    {
                        AddNewBoll(i, _sizeBord - 1, new Vector2(0, 1 + GetMaxOffset(i)), "");
                        flagDrop = true;
                    }
                }
            }
        }

        private int TestLine(bool showPoint = true)
        {
            int result = 0;
            for (int i = 0; i < _sizeBord; i++)
                for (int j = 0; j < _sizeBord; j++)
                {
                    if (_bord[i, j] == null || _bord[i, j].del)
                        continue;

                    int countBollX = 0;
                    int countBollY = 0;

                    for (int dx = 1; i + dx < _sizeBord; dx++)
                        if (_bord[i + dx, j] != null && _bord[i, j].typeBoll == _bord[i + dx, j].typeBoll)
                            countBollX++;
                        else
                            break;

                    if (countBollX > 1)
                    {
                        _posFlipBoll1.x = -1;
                        _flagAnimation = true;
                        for (int dx = 0; dx < countBollX + 1; dx++)
                            _bord[i + dx, j].del = true;
                    }

                    for (int dx = 1; j + dx < _sizeBord; dx++)
                        if (_bord[i, j + dx] != null && _bord[i, j].typeBoll == _bord[i, j + dx].typeBoll)
                            countBollY++;
                        else
                            break;

                    if (countBollY > 1)
                    {
                        _posFlipBoll1.x = -1;
                        _flagAnimation = true;
                        for (int dx = 0; dx < countBollY + 1; dx++)
                            _bord[i, j + dx].del = true;
                    }
                    if (countBollX < 2)
                        countBollX = 0;
                    if (countBollY < 2)
                        countBollY = 0;
                    int addPoint = 0;
                    if (countBollX + countBollY > 1)
                        addPoint = 10 + (countBollX + countBollY - 2) * 5;
                    if (addPoint > 0)
                    {
                        if (showPoint)
                            _addPointTextView.SetText("add point: " + addPoint);
                        result += addPoint;
                    }
                }
            return result;
        }

        private void Animation()
        {
            for (int i = 0; i < _sizeBord; i++)
                for (int j = 0; j < _sizeBord; j++)
                {
                    if (_bord[i, j] == null)
                        continue;

                    if (_bord[i, j].offset.magnitude > 0.001f)
                    {
                        _flagAnimation = true;
                        var delta = _bord[i, j].offset.normalized * Time.deltaTime * _speed;
                        if (delta.magnitude > _bord[i, j].offset.magnitude)
                            _bord[i, j].offset = Vector3.zero;
                        else
                            _bord[i, j].offset -= delta;
                    }
                    else
                    {
                        _bord[i, j].offset = Vector3.zero;
                    }
                    if (_bord[i, j].del)
                    {
                        float scale = _bord[i, j].transform.localScale.x;
                        scale -= Time.deltaTime * _speed;
                        if (scale < 0)
                            scale = 0;
                        else
                            _flagAnimation = true;

                        _bord[i, j].transform.localScale = new Vector3(scale, scale, scale);
                    }
                    _bord[i, j].transform.localPosition = new Vector3(i - _sizeBord / 2, j - _sizeBord / 2, 0) + _bord[i, j].offset;
                }
        }

        private void SelectBoll()
        {
            var posV3 = Input.mousePosition;
            var v2 = Camera.main.ScreenToWorldPoint(posV3);
            int x = (int)Mathf.Round(v2.x + _sizeBord / 2 - 0.5f);
            int y = (int)Mathf.Round(v2.y + _sizeBord / 2 - 0.5f);

            if (_selectBoll != null)
            {
                _selectBoll.sprite.color = Color.white;
            }

            if (x >= 0 && y >= 0 && x < _sizeBord && y < _sizeBord && (_selectPos.x != x || _selectPos.y != y))
            {
                if (_selectBoll != null && ((Mathf.Abs(x - _selectPos.x) < 2 && y == _selectPos.y) || (Mathf.Abs(y - _selectPos.y) < 2 && x == _selectPos.x)))
                {

                    FlipBoll(_bord[_selectPos.x, _selectPos.y].pos, _bord[x, y].pos);

                    _selectPos.x = -10;
                    _selectPos.y = -10;
                }
                else
                {
                    _selectBoll = _bord[x, y];
                    if (_selectBoll != null)
                    {
                        _selectBoll.sprite.color = Color.red;
                        _selectPos.x = x;
                        _selectPos.y = y;
                    }
                }
            }
            else
            {
                _selectPos.x = -10;
                _selectPos.y = -10;
            }
        }

        private void FlipBoll(PosBoll pos1, PosBoll pos2)
        {
            if (_bord[pos1.x, pos1.y] == null || _bord[pos2.x, pos2.y] == null)
                return;
            Boll temp = _bord[pos1.x, pos1.y];
            _posFlipBoll1 = pos1;
            _posFlipBoll2 = pos2;
            _bord[pos1.x, pos1.y] = _bord[pos2.x, pos2.y];
            _bord[pos2.x, pos2.y] = temp;

            _bord[pos1.x, pos1.y].pos = pos1;
            _bord[pos2.x, pos2.y].pos = pos2;

            _bord[pos1.x, pos1.y].offset = new Vector2(pos2.x - pos1.x, pos2.y - pos1.y);
            _bord[pos2.x, pos2.y].offset = new Vector2(pos1.x - pos2.x, pos1.y - pos2.y);
        }

        private void AddNewBoll(int x, int y, Vector2 offset, string name)
        {
            int typeBoll = Random.Range(0, _countBolls);
            while (TestBoll(typeBoll, x, y))
            {
                typeBoll++;
                if (typeBoll > _countBolls - 1)
                    typeBoll = 0;
            }
            _bord[x, y] = GameObject.Instantiate<Boll>(_bolls[typeBoll], _rootTransform);
            _bord[x, y].typeBoll = typeBoll;
            _bord[x, y].del = false;
            _bord[x, y].offset = offset;
            _bord[x, y].pos.x = x;
            _bord[x, y].pos.y = y;
            _bord[x, y].name = name;
            _bord[x, y].transform.localPosition = new Vector3(x - _sizeBord / 2, y - _sizeBord / 2, 0) + _bord[x, y].offset;
        }

        private bool TestBoll(int typeBoll, int x, int y)
        {
            if (x >= 2 && _bord[x - 1, y] != null && _bord[x - 2, y] != null)
                if (_bord[x - 1, y].typeBoll == typeBoll && _bord[x - 2, y].typeBoll == typeBoll)
                    return true;

            if (y >= 2 && _bord[x, y - 1] != null && _bord[x, y - 2] != null)
                if (_bord[x, y - 1].typeBoll == typeBoll && _bord[x, y - 2].typeBoll == typeBoll)
                    return true;

            return false;
        }

        private bool TestEndGame()
        {
            int points = TestLine(false);
            ResetDeleteFlagAnimFlip();

            if (points > 0)
                return false;

            for (int i = 0; i < _sizeBord - 1; i++)
                for (int j = 0; j < _sizeBord; j++)
                {
                    if (_bord[i, j] == null)
                        continue;
                    points = 0;
                    if (i + 1 < _sizeBord && _bord[i + 1, j] != null)
                    {
                        FlipBoll(_bord[i, j].pos, _bord[i + 1, j].pos);
                        points += TestLine(false);
                        FlipBoll(_bord[i, j].pos, _bord[i + 1, j].pos);
                        ResetDeleteFlagAnimFlip();
                    }
                    if (j + 1 < _sizeBord && _bord[i, j + 1] != null)
                    {
                        FlipBoll(_bord[i, j].pos, _bord[i, j + 1].pos);
                        points += TestLine(false);
                        FlipBoll(_bord[i, j].pos, _bord[i, j + 1].pos);
                        ResetDeleteFlagAnimFlip();
                    }

                    if (points > 0)
                    {
                        ResetDeleteFlagAnimFlip();
                        return false;
                    }
                }
            ResetDeleteFlagAnimFlip();
            return true;
        }

        private void ResetDeleteFlagAnimFlip()
        {
            _flagAnimation = false;
            _posFlipBoll1.x = -1;
            for (int i = 0; i < _sizeBord; i++)
                for (int j = 0; j < _sizeBord; j++)
                {
                    if (_bord[i, j] != null)
                    {
                        _bord[i, j].del = false;
                        _bord[i, j].offset = Vector3.zero;
                    }
                }
        }

        private float GetMaxOffset(int x)
        {
            for (int j = _sizeBord - 1; j >= 0; j--)
            {
                if (_bord[x, j] != null)
                {
                    if (_sizeBord > j + _bord[x, j].offset.y)
                        return 1;
                    else
                        return j + _bord[x, j].offset.y - _sizeBord + 1;
                }
            }
            return 0;
        }
    }
}