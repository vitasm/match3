﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddPointTextView : MonoBehaviour
{
    [SerializeField] private Text _text;
    private float _leftTime;

    public void SetText(string text, float showTime = 1, bool replace = false)
    {
        if (_leftTime > 0 && !replace)
        {
            _text.text += "\n" + text;
        }
        else
        {
            _text.text = text;
        }
        _leftTime = showTime;
    }

    private void Update()
    {
        _leftTime -= Time.deltaTime;
        if (_leftTime < 0)
            _text.text = "";
    }
}
